from datetime import datetime
import os

# initialize the django environment
os.environ['DJANGO_SETTINGS_MODULE'] = 'mofo.settings'
import django
django.setup()


from account.models import FomoUser

#need three of these bad boys
# u1 = FomoUser()
# u1.username = 'juancho'
# u1.first_name = 'Juan'
# u1.first_name = 'Jones'
# u1.set_password('nachos')
# u1.city = 'New York'
# u1.save()
#
# u2 = FomoUser()
# u2.username = 'jimbo'
# u2.first_name = 'Jim'
# u2.first_name = "Johnson"
# u2.city = 'Atlanta'
# u2.set_password('chicken')
# u2.save()
#
# u3 = FomoUser()
# u3.username = 'sirCumference'
# u3.first_name = 'William'
# u3.first_name = "Cumference"
# u3.city = 'New York'
# u3.set_password('twopiesquared')
# u3.save()

#query all/some with different query OPTIONS
#3-5 examples

query1 = FomoUser.objects.filter(city='New York')

print('')
print('this is the first query')

for u1 in query1:
    print(u1.username, u1.city)
print('')
print('this is the second query')
query2 = FomoUser.objects.all()
print('these are the hashed passwords (look how safe we are!):')
for poop in query2:
    print(poop.password)

print('')
print('this is the third query')
query3 = FomoUser.objects.filter(first_name__startswith='J')
for poopoo in query3:
    print(poopoo.first_name)
