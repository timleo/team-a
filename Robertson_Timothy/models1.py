from django.db import models
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.models import (
    AbstractUser, PermissionsMixin
)
from django.contrib.auth.base_user import AbstractBaseUser

# ACCOUNT APP

class FomoUser(AbstractUser):
    #inherits username, first/last_name, email, date_joined, is_staff (whether user can log into admin)
    #is_active from abstractUser
    #inherits password, last_login, is_active from abstractbaseuser
    #methods as well - get_username, save, setpassword, check_password,
    #get_full_name, get_short_name,
    address = models.CharField(('Address'), max_length=200, blank=True)
    city = models.CharField(('City'), max_length=50, blank=True)
    STATE_CHOICES = (
    ('AL', 'Alabama'),
    ('AZ', 'Arizona'),
    ('AR', 'Arkansas'),
    ('CA', 'California'),
    ('CO', 'Colorado'),
    ('CT', 'Connecticut'),
    ('DE', 'Delaware'),
    ('DC', 'District of Columbia'),
    ('FL', 'Florida'),
    ('GA', 'Georgia'),
    ('ID', 'Idaho'),
    ('IL', 'Illinois'),
    ('IN', 'Indiana'),
    ('IA', 'Iowa'),
    ('KS', 'Kansas'),
    ('KY', 'Kentucky'),
    ('LA', 'Louisiana'),
    ('ME', 'Maine'),
    ('MD', 'Maryland'),
    ('MA', 'Massachusetts'),
    ('MI', 'Michigan'),
    ('MN', 'Minnesota'),
    ('MS', 'Mississippi'),
    ('MO', 'Missouri'),
    ('MT', 'Montana'),
    ('NE', 'Nebraska'),
    ('NV', 'Nevada'),
    ('NH', 'New Hampshire'),
    ('NJ', 'New Jersey'),
    ('NM', 'New Mexico'),
    ('NY', 'New York'),
    ('NC', 'North Carolina'),
    ('ND', 'North Dakota'),
    ('OH', 'Ohio'),
    ('OK', 'Oklahoma'),
    ('OR', 'Oregon'),
    ('PA', 'Pennsylvania'),
    ('RI', 'Rhode Island'),
    ('SC', 'South Carolina'),
    ('SD', 'South Dakota'),
    ('TN', 'Tennessee'),
    ('TX', 'Texas'),
    ('UT', 'Utah'),
    ('VT', 'Vermont'),
    ('VA', 'Virginia'),
    ('WA', 'Washington'),
    ('WV', 'West Virginia'),
    ('WI', 'Wisconsin'),
    ('WY', 'Wyoming'))
    state = models.CharField(max_length=2, choices=STATE_CHOICES)



#here are all the usefull things that we inherit from abstractbaseuser

# class AbstractBaseUser(models.Model):
#     password = models.CharField(_('password'), max_length=128)
#     last_login = models.DateTimeField(_('last login'), blank=True, null=True)
#
#     is_active = True
#
#     REQUIRED_FIELDS = []
#
#     def get_username(self):
#         "Return the identifying username for this User"
#         return getattr(self, self.USERNAME_FIELD)
#
#     def set_password(self, raw_password):
#         self.password = make_password(raw_password)
#         self._password = raw_password
#
#     def check_password(self, raw_password):
#         """
#         Return a boolean of whether the raw_password was correct. Handles
#         hashing formats behind the scenes.
#         """
#         def setter(raw_password):
#             self.set_password(raw_password)
#             # Password hash upgrades shouldn't be considered password changes.
#             self._password = None
#             self.save(update_fields=["password"])
#         return check_password(raw_password, self.password, setter)
#
#     def get_full_name(self):
#         raise NotImplementedError('subclasses of AbstractBaseUser must provide a get_full_name() method')
#
#     def get_short_name(self):
#         raise NotImplementedError('subclasses of AbstractBaseUser must provide a get_short_name() method.')
#
#     @classmethod
#     def get_email_field_name(cls):
#         try:
#             return cls.EMAIL_FIELD
#         except AttributeError:
#             return 'email'
#
#     @classmethod
#     def normalize_username(cls, username):
#         return unicodedata.normalize('NFKC', force_text(username))

###here is all the stuff we inherit from abstractuser

# class AbstractUser(AbstractBaseUser, PermissionsMixin):
#     """
#     An abstract base class implementing a fully featured User model with
#     admin-compliant permissions.
#     Username and password are required. Other fields are optional.
#     """
#     username_validator = UnicodeUsernameValidator()
#
#     username = models.CharField(
#         _('username'),
#         max_length=150,
#         unique=True,
#         help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
#         validators=[username_validator],
#         error_messages={
#             'unique': _("A user with that username already exists."),
#         },
#     )
#     first_name = models.CharField(_('first name'), max_length=30, blank=True)
#     last_name = models.CharField(_('last name'), max_length=30, blank=True)
#     email = models.EmailField(_('email address'), blank=True)
#     is_staff = models.BooleanField(
#         _('staff status'),
#         default=False,
#         help_text=_('Designates whether the user can log into this admin site.'),
#     )
#     is_active = models.BooleanField(
#         _('active'),
#         default=True,
#         help_text=_(
#             'Designates whether this user should be treated as active. '
#             'Unselect this instead of deleting accounts.'
#         ),
#     )
#     date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
#
#     objects = UserManager()
#
#     EMAIL_FIELD = 'email'
#     USERNAME_FIELD = 'username'
#     REQUIRED_FIELDS = ['email']
#
#     def email_user(self, subject, message, from_email=None, **kwargs):
#         """
#         Sends an email to this User.
#         """
#         send_mail(subject, message, from_email, [self.email], **kwargs)
