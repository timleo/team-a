#
#      this is what i did in class but it wasnt working
# def Last5ProductsMiddleware(get_response):
#
#     def middleware(request):
#         #code to be executed for each request before
#         #the view (and later middleware) are called
#         #get the last 5 from the request.session
#         request.last5 = request.session.get('last5')
#         if request.last5 is None:
#             request.last5 = []
#
#
#         #let django continue the processing
#         #django will call your view function here
#         response = get_response(request)
#
#         request.session('last5') = request.last5[:5]
#
#
#         print('>>>>>>>>>>> middleware called foo')
#         return response
#
#     return middleware

def Last5ProductsMiddleware(get_response):
    def middleware(request):
        request.last5 = request.session.get('last5')
        if request.last5 is None:
            request.last5 = []


        response = get_response(request)
        print('middleware is working yo')

        request.session['last5'] = request.last5[:5]
        return response

    return middleware
