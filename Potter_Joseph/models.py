from django.db import models
from django.contrib.auth.models import AbstractUser
# Define models here
#account app
class FomoUser(AbstractUser):
    Phone = models.CharField(max_length = 15)
    Address = models.CharField(max_length = 100)
    City = models.CharField(max_length = 100)
    State  = models.CharField(max_length = 2)
    #here is a change that I am making
