from django.conf import settings
from django.http import HttpResponseRedirect
from django_mako_plus import view_function
from catalog import models as cmod
from .. import dmp_render, dmp_render_to_string

@view_function
def process_request(request):
    pid = request.urlparams[0]
    products = cmod.Product.objects.order_by('name').all()
    last5prod = []
    try:
        product = cmod.Product.objects.get(id=pid)
    except cmod.Product.DoesNotExist:
        return HttpResponseRedirect('/catalog/index/')
    for id in request.last5:
       product5 = cmod.Product.objects.get(id=id)
       last5prod.append(product5)#the list gets tossed to the details page before being updated.
    for p in request.last5:
        if p == product.id:
            request.last5.remove(product.id)
    request.last5.insert(0,product.id)
    while len(request.last5) > 5:
        request.last5.pop()
    return dmp_render(request,'detail.html',{
    'last5prod':last5prod,
    'product':product,
    'products':products,
    })

#Added this comment to fix mistake
