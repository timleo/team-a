from django.db import models
from polymorphic.models import PolymorphicModel
from account import models as amod

# Create your models here.

class Category(models.Model):
    #id
    codename = models.TextField(blank = True, null = True)
    name = models.TextField(blank = True, null = True)
    def __str__(self):
        return  self.name
class Product(PolymorphicModel):
    #id
    name = models.TextField(blank = True, null = True)
    category = models.ForeignKey('Category')
    price = models.DecimalField(max_digits = 8, decimal_places = 2)
    create_date = models.DateTimeField(auto_now_add = True)
    modified_date = models.DateTimeField(auto_now = True)
    description = models.TextField(max_length = 500)
class BulkProduct(Product):
    #id
    #name
    #Category
    #price
    #create_date
    #modified_date
    quantity = models.IntegerField()
    reorder_trigger = models.IntegerField()
    reorder_quantity = models.IntegerField()
class UniqueProduct(Product):
    #id
    #name
    #Category
    #price
    #create_date
    #modified_date
    Serial_number = models.TextField(blank = True, null = True)
class RentalProduct(Product):
    #id
    #name
    #Category
    #price
    #create_date
    #modified_date
    Serial_number = models.TextField(blank = True, null = True)
class ProductPicture(models.Model):
    #id
    path = models.TextField()
    is_main = models.BooleanField()
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
class sale(models.Model):
    date = models.DateTimeField(auto_now_add = True)
    subtotal = models.DecimalField()
    total = models.DecimalField()
    isFullyPaid =models.BooleanField(default = False)
    user = models.ForeignKey(amod.FomoUser, on_delete = models.CASCADE)
    Address = models.TextField()
    City = models.TextField()
    State = models.TextField()
    Zip = models.TextField()
    def recordSale(User, Address, City, State, Zip, PaymentAmount):
        sale = sale()
        sale.user = User
        sale.Address = Address
        Sale.City = City
        Sale.State = State
        Sale.Zip = Zip
        subtotal = 0
        items = KartItem.objects.flter(user = User, isActive = True)
        for i in items:
            product = i.product
            SI = SaleItem()
            SI.product = i.product
            SI.quantity = i.quantity
            salePrice = product.price * SI.quantity
            subtotal = subtotal + saleprice
            SI.sale = i.sale
            if product.Serial_number:
                product.delete()
            else:
                product.quantity = product.quantity - SI.quantity
                product.save()
            SI.save()
        sale.subtotal = subtotal
        sale.total = subtotal*1.0725
        p = payment()
        p.amount = PaymentAmount
        p.sale = sale
        if PaymentAmount >= sale.total:
            sale.isFullyPaid = True
        p.save()
        sale.save()


class saleItem(models.Model):
    salePrice = models.DecimalFeild()
    quantity = models.IntegerField(default = 1)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    sale = models.ForeignKey(sale, on_delete=models.CASCADE)
class payment(models.Model):
    amount = models.DecimalFeild()
    sale = models.ForeignKey(sale, on_delete=models.CASCADE)
class cartItem(models.Model):
    quantity = models.IntegerField(default = 1)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    isActive = models.BooleanField(default = True)
    user = models.ForeignKey(amod.FomoUser, on_delete = models.CASCADE)
