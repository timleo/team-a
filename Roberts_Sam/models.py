from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.base_user import AbstractBaseUser

# ACCOUNT APP

class FomoUser(AbstractUser):
    Phone = models.CharField(max_length = 15)
    Address = models.CharField(max_length = 100)
    City = models.CharField(max_length = 100)
    State  = models.CharField(max_length = 2)


#This FomoUser will inherit from AbstractUser and AbstractBaseUser
#Tim has the code for those two to show what FomoUser is actually inheriting
