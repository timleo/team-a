from django.db import models
from polymorphic.models import PolymorphicModel

# Create your models here.

class Category(models.Model):
    #id
    codename = models.TextField(blank=True,null=True)
    name = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name

class Product(PolymorphicModel):
    #id
    name = models.TextField(blank=True, null=True)
    category = models.ForeignKey('Category')
    description = models.TextField(blank=True, null=True)
    price = models.DecimalField(max_digits=8, decimal_places=2) #999,999.99
    create_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

class ProductPicture(models.Model):
    product = models.ForeignKey(Product)
    path = models.TextField(null=True) #will have string to go where the picture is stored
    mainpic = models.NullBooleanField()

class BulkProduct(Product):
    #id
    quantity = models.IntegerField(default=0)
    reorder_trigger = models.IntegerField(default=0)
    reorder_quantity = models.IntegerField(default=0)

class UniqueProduct(Product):
    #id
    serial_number = models.TextField()
    #vendor info

class RentalProduct(Product):
    #id
    serial_number = models.TextField(blank=True,null=True)

class sale(models.Model):
    date = models.DateTimeField(auto_now_add = True)
    subtotal = models.DecimalField()
    total = models.DecimalField()
    isFullyPaid =models.BooleanField(default = False)
    user = models.ForeignKey(amod.FomoUser, on_delete = models.CASCADE)
    Address = models.TextField()
    City = models.TextField()
    State = models.TextField()
    Zip = models.TextField()
    def recordSale(User, Address, City, State, Zip, PaymentAmount):
        sale = sale()
        sale.user = User
        sale.Address = Address
        Sale.City = City
        Sale.State = State
        Sale.Zip = Zip
        subtotal = 0
        items = KartItem.objects.flter(user = User, isActive = True)
        for i in items:
            product = i.product
            SI = SaleItem()
            SI.product = i.product
            SI.quantity = i.quantity
            salePrice = product.price * SI.quantity
            subtotal = subtotal + saleprice
            SI.sale = i.sale
            if product.Serial_number:
                product.delete()
            else:
                product.quantity = product.quantity - SI.quantity
                product.save()
            SI.save()
        sale.subtotal = subtotal
        sale.total = subtotal*1.0725
        p = payment()
        p.amount = PaymentAmount
        p.sale = sale
        if PaymentAmount >= sale.total:
            sale.isFullyPaid = True
        p.save()
        sale.save()


class SaleItem(models.Model):
    salePrice = models.DecimalFeild()
    quantity = models.IntegerField(default = 1)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    sale = models.ForeignKey(sale, on_delete=models.CASCADE)
class payment(models.Model):
    amount = models.DecimalFeild()
    sale = models.ForeignKey(sale, on_delete=models.CASCADE)
class KartItem(models.Model):
    quantity = models.IntegerField(default = 1)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    isActive = models.BooleanField(default = True)
    user = models.ForeignKey(amod.FomoUser, on_delete = models.CASCADE)
