from django.core import management

from django.db import connection
from datetime import datetime
import os, os.path, sys


areyousure = input('''
  You are about to drop and recreate the entire database.
  All data are about to be deleted.  Use of this script
  may cause itching, vertigo, dizziness, tingling in
  extremities, loss of balance or coordination, slurred
  speech, temporary zoobie syndrome, longer lines at the
  testing center, changed passwords in Learning Suite, or
  uncertainty about whether to call your professor
  'Brother' or 'Doctor'.

  Please type 'yes' to confirm the data destruction: ''')
if areyousure.lower() != 'yes':
    print()
    print('  Wise choice.')
    sys.exit(1)

# initialize the django environment
os.environ['DJANGO_SETTINGS_MODULE'] = 'fomo.settings'
import django
django.setup()


# drop and recreate the database tables
print()
print('Living on the edge!  Dropping the current database tables.')
with connection.cursor() as cursor:
    cursor.execute("DROP SCHEMA public CASCADE")
    cursor.execute("CREATE SCHEMA public")
    cursor.execute("GRANT ALL ON SCHEMA public TO postgres")
    cursor.execute("GRANT ALL ON SCHEMA public TO public")

# make the migrations and migrate
management.call_command('makemigrations')
management.call_command('migrate')

from account.models import FomoUser


f1 = FomoUser()
f1.username = "markiscool"
f1.first_name = "mark"
f1.last_name = "mcawesome"
f1.email = "mark@yeahright.com"
f1.Address = "123 evergreen terrace"
f1.Phone = "1234656754"
f1.City = "americajr"
f1.state = "DC"
f1.save()

f2 = FomoUser()
f2.username = "carldaman"
f2.first_name = "Carl"
f2.last_name = "Sagan"
f2.email = "carl@science.ccm"
f2.Address = "america place"
f2.Phone = "1234567891"
f2.City = "provo"
f2.state = "UT"
f2.save()

f3 = FomoUser()
f3.username = "timleo"
f3.first_name = "Tim"
f3.last_name = "Robertson"
f3.email = "tim@robertson.com"
f3.Address = "123 northstreet"
f3.Phone = "3013454323
f3.City = "provo"
f3.state = "UT"
f3.save()


f4 = FomoUser.objects.get(username = 'timleo')
print(f4.first_name   f4.last_name)
FomoUsers = FomoUser.objects.filter(Address = '123 northstreet')
for f in FomoUsers:
    print(f.first_name   f.last_name)
FomoUsers = FomoUser.objects.all()
for f in FomoUsers:
    print(f.first_name   f.last_name)
